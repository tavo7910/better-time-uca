package com.proyectofinal.bettertime.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class Materias extends RealmObject implements Parcelable {

    public static final String TAG = "materia_tag";

    @PrimaryKey
    private int id;

    private String nombreMateria;

    public Materias() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreMateria() {
        return nombreMateria;
    }

    public void setNombreMateria(String nombreMateria) {
        this.nombreMateria = nombreMateria;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nombreMateria);
    }

    protected Materias(Parcel in) {
        id = in.readInt();
        nombreMateria = in.readString();
    }

    public static final Creator<Materias> CREATOR = new Creator<Materias>() {
        @Override
        public Materias createFromParcel(Parcel in) {
            return new Materias(in);
        }

        @Override
        public Materias[] newArray(int size) {
            return new Materias[size];
        }
    };
}
