package com.proyectofinal.bettertime.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class DaysEvent extends RealmObject implements Parcelable {

    public static final String TAG = "DaysEvent";

    @PrimaryKey
    private int id;
    private String nombreEvento;
    private String horaEvento;
    private String diaEvento;
    private String materia;

    public DaysEvent() {

    }

    public DaysEvent(String nombre, String hora, String dia, String materia) {
        this.nombreEvento = nombre;
        this.horaEvento = hora;
        this.diaEvento = dia;
        this.materia = materia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public String getHoraEvento() {
        return horaEvento;
    }

    public void setHoraEvento(String horaEvento) {
        this.horaEvento = horaEvento;
    }

    public String getDiaEvento() {
        return diaEvento;
    }

    public void setDiaEvento(String diaEvento) {
        this.diaEvento = diaEvento;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.nombreEvento);
        dest.writeString(this.horaEvento);
        dest.writeString(this.diaEvento);
        dest.writeString(this.materia);
    }

    protected DaysEvent(Parcel in) {
        this.id = in.readInt();
        this.nombreEvento = in.readString();
        this.horaEvento = in.readString();
        this.diaEvento = in.readString();
        this.materia = in.readString();
    }

    public static final Parcelable.Creator<DaysEvent> CREATOR = new Parcelable.Creator<DaysEvent>() {
        @Override
        public DaysEvent createFromParcel(Parcel source) {
            return new DaysEvent(source);
        }

        @Override
        public DaysEvent[] newArray(int size) {
            return new DaysEvent[size];
        }
    };
}
