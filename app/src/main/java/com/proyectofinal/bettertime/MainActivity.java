package com.proyectofinal.bettertime;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity {

    private BottomBar mBottomBarNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBottomBarNav = (BottomBar) findViewById(R.id.bottomNavigation);
        inflateScreenFragment();
    }

    private void inflateScreenFragment() {
        mBottomBarNav.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.menu_home:
                        HomeFragment homeFragment = new HomeFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.base_fragment_layout, homeFragment).commit();
                        break;
                    case R.id.menu_events:
                        EventsFragment horariosFragment = new EventsFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.base_fragment_layout, horariosFragment).commit();
                        break;
                    case R.id.menu_subjects:
                        MateriasFragment materiasFragment = new MateriasFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.base_fragment_layout, materiasFragment).commit();
                        break;
                    case R.id.menu_profile:
                        PerfilFragment perfilFragment = new PerfilFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.base_fragment_layout, perfilFragment).commit();
                        break;
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 334:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1313);
                    Log.e("value", "Gracias por habilitar los permisos.");
                } else {
                    Log.e("value", "Permiso denegado, lo sentimos.");
                }
                break;
        }
    }
}
