package com.proyectofinal.bettertime;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class HomeFragment extends Fragment {

    private ViewPager mViewPagerContainer;
    private TabLayout mDaysTab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(getString(R.string.menu_home));

        mViewPagerContainer = (ViewPager) getActivity().findViewById(R.id.days_view_pager_container);
        mDaysTab = (TabLayout) getActivity().findViewById(R.id.days_table_layout);

        ViewPagerHomeAdapter viewPagerHomeAdapter = new ViewPagerHomeAdapter(getChildFragmentManager());
        mViewPagerContainer.setAdapter(viewPagerHomeAdapter);
        mDaysTab.setupWithViewPager(mViewPagerContainer);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_event_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_new_event) {
            Intent intent = new Intent(getActivity(), CreateEvent.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
