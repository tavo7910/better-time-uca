package com.proyectofinal.bettertime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.proyectofinal.bettertime.model.Perfiles;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import io.realm.Realm;

public class ActivityUpdateProfile extends AppCompatActivity implements View.OnClickListener {

    ImageView imgTakenPic;
    private static final int CAM_REQUEST = 1313;
    private EditText userName;
    private EditText carnet;
    private Button guardar;
    private String photoPath;
    private File finalFile;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        setTitle(getString(R.string.menu_profile));

        userName = (EditText) findViewById(R.id.nombre_user_editText);
        carnet = (EditText) findViewById(R.id.editTextCarnet);
        guardar = (Button) findViewById(R.id.btnGuardarPerfil);
        imgTakenPic = (ImageView) findViewById(R.id.imageView);
        imgTakenPic.setOnClickListener(this);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userName.getText().toString().equals("") && !carnet.getText().toString().equals("")) {
                    saveUserInfo();
                    Toast.makeText(ActivityUpdateProfile.this, "Perfil actualizado", Toast.LENGTH_SHORT).show();
                    finish();
                }else {
                    Toast.makeText(ActivityUpdateProfile.this, "Rellenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }

        });

        if (getUsuarios() != null) {
            loadUserInfo(getUsuarios());
        }

        Log.d("statePerfil", "usuarios: " + getUsuarios());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == CAM_REQUEST) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imgTakenPic.setImageBitmap(bitmap);

            Uri tempUri = getImageUri(getApplicationContext(), bitmap);
            finalFile = new File(getRealPathFromURI(tempUri));
            Log.d("imgRuta", "ruta: " + finalFile + " fotoPath: " + photoPath);
        }
    }

    private void saveUserInfo() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Perfiles perfil = new Perfiles();

        perfil.setId(1);
        perfil.setUserName(userName.getText().toString());
        perfil.setCarnet(carnet.getText().toString());
        if (finalFile!=null) {
            perfil.setPhoto(finalFile.toString());
        }else {
            perfil.setPhoto("nothing");
        }
        realm.copyToRealmOrUpdate(perfil);
        realm.commitTransaction();
    }

    private List<Perfiles> getUsuarios() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Perfiles.class).equalTo("id", 1).findAll();
    }

    private List<Perfiles> loadUserInfo(List<Perfiles> perfiles) {
        for (Perfiles profile : perfiles) {
            userName.setText(profile.getUserName());
            carnet.setText(profile.getCarnet());

            bitmap = BitmapFactory.decodeFile(profile.getPhoto());
            Log.d("status", "bitmap not null");
            imgTakenPic.setImageBitmap(bitmap);
        }
        return perfiles;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream best = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, best);
        photoPath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(photoPath);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(ActivityUpdateProfile.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityUpdateProfile.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(ActivityUpdateProfile.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 334);
            Toast.makeText(ActivityUpdateProfile.this, "Si quieres tomar fotos, concedele permisos a BetterTime", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(ActivityUpdateProfile.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 334);
        }
    }


    public void onClick(View v) {
        if (v.getId() == R.id.imageView) {
            if (checkPermission()) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAM_REQUEST);
            }else {
                requestPermission();
            }
        }
    }


}
