package com.proyectofinal.bettertime;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proyectofinal.bettertime.model.DaysEvent;

import java.util.List;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class DaysEventAdapter extends RecyclerView.Adapter<DaysEventAdapter.ViewHolder> {

    private List<DaysEvent> dayList;
    private OnEventClicked mListener;

    public DaysEventAdapter(List<DaysEvent> dayList, OnEventClicked listener) {
        this.dayList = dayList;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_days, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DaysEvent daysEvent = dayList.get(position);
        holder.nombreEvento.setText(daysEvent.getNombreEvento());
        holder.horaEvento.setText(daysEvent.getHoraEvento());
        holder.materiaEvento.setText(daysEvent.getMateria());
        holder.diaEvento.setText(daysEvent.getDiaEvento());

        holder.eventListener(dayList.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nombreEvento, horaEvento, materiaEvento, diaEvento;

        public ViewHolder(View itemView) {
            super(itemView);
            nombreEvento = (TextView) itemView.findViewById(R.id.nombre_evento);
            horaEvento = (TextView) itemView.findViewById(R.id.hora_evento);
            materiaEvento = (TextView) itemView.findViewById(R.id.materia_evento);
            diaEvento = (TextView) itemView.findViewById(R.id.dia_evento);
        }

        private void eventListener(final DaysEvent eventDays, final OnEventClicked listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onEventTapped(eventDays, getAdapterPosition());
                }
            });
        }
    }

    public interface OnEventClicked {
        void onEventTapped(DaysEvent daysEvent, int position);
    }
}
