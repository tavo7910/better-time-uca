package com.proyectofinal.bettertime;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proyectofinal.bettertime.model.DaysEvent;

import java.util.List;

import io.realm.Realm;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class EventsFragment extends Fragment implements DaysEventAdapter.OnEventClicked {

    private RecyclerView allEventsRecyclerView;
    private DaysEventAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.menu_event));

        allEventsRecyclerView = (RecyclerView) getActivity().findViewById(R.id.rv_all_event_list);
        adapter = new DaysEventAdapter(loadAllEventList(), this);
        allEventsRecyclerView.setAdapter(adapter);
        Log.d("EventList", "Event saved: " + loadAllEventList());
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new DaysEventAdapter(loadAllEventList(), this);
        allEventsRecyclerView.setAdapter(adapter);
        Log.d("EventList", "Event saved: " + loadAllEventList());
    }

    private List<DaysEvent> loadAllEventList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DaysEvent.class).findAllSorted("horaEvento");
    }

    @Override
    public void onEventTapped(DaysEvent daysEvent, int position) {
        Intent intent = new Intent(getActivity(), UpdateEvento.class);
        intent.putExtra(DaysEvent.TAG, daysEvent);
        startActivity(intent);
    }
}
