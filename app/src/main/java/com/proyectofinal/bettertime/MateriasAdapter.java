package com.proyectofinal.bettertime;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proyectofinal.bettertime.model.DaysEvent;
import com.proyectofinal.bettertime.model.Materias;

import java.util.List;

import io.realm.Realm;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class MateriasAdapter extends RecyclerView.Adapter<MateriasAdapter.ViewHolder> {


    private List<Materias> materiasList;
    private OnEventClicked mListener;
    private int adapterPosition;

    public MateriasAdapter(List<Materias> materias, MateriasAdapter.OnEventClicked listener) {
        this.materiasList = materias;
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_materia, parent, false);
        return new MateriasAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Materias materias = materiasList.get(position);
        holder.nombreMateria.setText(materias.getNombreMateria());
        holder.eventListener(materias, mListener);

    }

    @Override
    public int getItemCount() {
        return materiasList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nombreMateria;

        public ViewHolder(View itemView) {
            super(itemView);
            nombreMateria = (TextView) itemView.findViewById(R.id.materia_id);
        }

        private void eventListener(final Materias materias, final MateriasAdapter.OnEventClicked listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onEventTapped(materias, getAdapterPosition());
                }
            });
        }
    }

    public interface OnEventClicked {
        void onEventTapped(Materias materias, int position);
    }

}
