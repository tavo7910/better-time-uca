package com.proyectofinal.bettertime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proyectofinal.bettertime.model.DaysEvent;
import com.proyectofinal.bettertime.model.Materias;

import io.realm.Realm;

public class CreateMateriaActivity extends AppCompatActivity {

    private Button btnSaveSubject;
    private EditText mNombreMateria;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_materia);
        btnSaveSubject = (Button) findViewById(R.id.btn_save_materia);
        mNombreMateria = (EditText) findViewById(R.id.materia_editText);


        btnSaveSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveSubject();
                Toast.makeText(CreateMateriaActivity.this, "Materia Guardada", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        }


    private void saveSubject() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Materias materia = new Materias();
        Number maxId = realm.where(Materias.class).max("id");
        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;

        materia.setId(nextId);
        materia.setNombreMateria(mNombreMateria.getText().toString());
        realm.copyToRealmOrUpdate(materia);
        realm.commitTransaction();
    }
}
