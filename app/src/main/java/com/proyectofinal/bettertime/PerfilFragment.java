package com.proyectofinal.bettertime;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.proyectofinal.bettertime.model.Perfiles;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import io.realm.Realm;

import static android.app.Activity.RESULT_OK;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class PerfilFragment extends Fragment {

    ImageView imgTakenPic;
    private TextView userName;
    private TextView carnet;
    private String photoPath;
    private File finalFile;
    Bitmap bitmap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_perfil, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.menu_profile));

        setHasOptionsMenu(true);

        userName = (TextView) getActivity().findViewById(R.id.nombre_user_editText);
        carnet = (TextView) getActivity().findViewById(R.id.editTextCarnet);
        imgTakenPic = (ImageView) getActivity().findViewById(R.id.imageView);

        loadUserInfo(getUsuarios());

    }

    @Override
    public void onResume() {
        super.onResume();
        loadUserInfo(getUsuarios());
    }

    private List<Perfiles> getUsuarios() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Perfiles.class).equalTo("id", 1).findAll();
    }


    private List<Perfiles> loadUserInfo(List<Perfiles> perfiles) {
        for (Perfiles profile : perfiles) {
            userName.setText(profile.getUserName());
            carnet.setText(profile.getCarnet());

            bitmap = BitmapFactory.decodeFile(profile.getPhoto());
            Log.d("status", "bitmap not null");
            imgTakenPic.setImageBitmap(bitmap);
        }
        return perfiles;
    }



    /** Show toolbar icons*/
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.update_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /** Handle actions for toolbar icons*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_updateProfile) {
            Intent intent = new Intent(getActivity(), ActivityUpdateProfile.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }






//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case 334:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent, CAM_REQUEST);
//                    Log.e("value", "Permission Granted, Now you can use local drive .");
//                } else {
//                    Log.e("value", "Permission Denied, You cannot use local drive .");
//                }
//                break;
//        }
//    }

}
