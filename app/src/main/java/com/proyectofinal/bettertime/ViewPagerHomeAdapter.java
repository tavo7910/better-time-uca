package com.proyectofinal.bettertime;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.proyectofinal.bettertime.fragmentDays.FridayFragment;
import com.proyectofinal.bettertime.fragmentDays.SaturdayFragment;
import com.proyectofinal.bettertime.fragmentDays.ThursdayFragment;
import com.proyectofinal.bettertime.fragmentDays.TuesdayFragment;
import com.proyectofinal.bettertime.fragmentDays.MondayFragment;
import com.proyectofinal.bettertime.fragmentDays.WednesdayFragment;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class ViewPagerHomeAdapter extends FragmentPagerAdapter {
    public ViewPagerHomeAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new MondayFragment();
                break;
            case 1:
                fragment = new TuesdayFragment();
                break;
            case 2:
                fragment = new WednesdayFragment();
            break;
            case 3:
                fragment = new ThursdayFragment();
                break;
            case 4:
                fragment = new FridayFragment();
                break;
            case 5:
                fragment = new SaturdayFragment();
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String pageTitle = null;
        switch (position) {
            case 0:
                pageTitle = "Lun";
                break;
            case 1:
                pageTitle = "Mar";
                break;
            case 2:
                pageTitle = "Mie";
                break;
            case 3:
                pageTitle = "Jue";
                break;
            case 4:
                pageTitle = "Vie";
                break;

            case 5:
                pageTitle = "Sab";
                break;
             /*
            case 6:
                pageTitle = "Dom";
                break;
                */
        }

        return pageTitle;
    }

    @Override
    public int getCount() {
        return 6;
    }
}
