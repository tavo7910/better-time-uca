package com.proyectofinal.bettertime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.proyectofinal.bettertime.model.DaysEvent;
import com.proyectofinal.bettertime.model.Materias;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class CreateEvent extends AppCompatActivity {

    private Button btnSaveEvento;
    private EditText mNombreEvento, mMateria;
    private Spinner mDiaSpinner;
    private Spinner mMateriaSpinner;
    private TimePicker mHora;
    private String format = "";
    private String mHour = "";
    private TextView mHoraView;
    private List<String> weekDays = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        setTitle(getString(R.string.crear_evento));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        btnSaveEvento = (Button) findViewById(R.id.btn_save_evento);
        mNombreEvento = (EditText) findViewById(R.id.nombre_evento_editText);
        //mMateria = (EditText) findViewById(R.id.materia_evento_editText);
        mDiaSpinner = (Spinner) findViewById(R.id.dia_evento_editText);
        mMateriaSpinner = (Spinner) findViewById(R.id.materia_evento_spinner);
        mHora = (TimePicker) findViewById(R.id.hora_evento_editText);
        mHoraView = (TextView) findViewById(R.id.hora_evento_textView);

        spinnerItems();
        spinnerMateriaItems();
        getMaterias(loadMateriasList());

        btnSaveEvento.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showHour();
                    saveEvento();
                    Toast.makeText(CreateEvent.this, "Evento guardado", Toast.LENGTH_SHORT).show();
                    finish();
                }
        });
    }

    private void spinnerItems() {
        weekDays.add("Lunes");
        weekDays.add("Martes");
        weekDays.add("Miercoles");
        weekDays.add("Jueves");
        weekDays.add("Viernes");
        weekDays.add("Sabado");

        ArrayAdapter<String> daysSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, weekDays);
        daysSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDiaSpinner.setAdapter(daysSpinnerAdapter);
    }

    private void spinnerMateriaItems() {
        ArrayAdapter<String> materiasSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                getMaterias(loadMateriasList()));
        materiasSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mMateriaSpinner.setAdapter(materiasSpinnerAdapter);
    }

    private List<Materias> loadMateriasList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Materias.class).findAll();
    }

    private List<String> getMaterias(List<Materias> materiasList) {
        List<String> materiass = new ArrayList<>();
        for (Materias materias : materiasList) {
            Log.d("materia", "materia list: " + materias.getNombreMateria());
            materiass.add(materias.getNombreMateria());
        }
        return materiass;
    }

    private void showHour() {
        int hour = mHora.getCurrentHour();
        int min = mHora.getCurrentMinute();
        setHourFormat(hour, min);
    }

    private void setHourFormat(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        //Log.d("timeFormatted", "hour is: " + new StringBuilder().append(hour).append(":").append(min).append(" ").append(format));
        mHoraView.setText(new StringBuilder().append(hour).append(":").append(min).append(" ").append(format));
        mHour = mHoraView.getText().toString();
    }

    private void saveEvento() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        DaysEvent event = new DaysEvent();
        Number maxId = realm.where(DaysEvent.class).max("id");
        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;

        event.setId(nextId);
        event.setNombreEvento(mNombreEvento.getText().toString());
        event.setMateria(mMateriaSpinner.getSelectedItem().toString());
        event.setDiaEvento(mDiaSpinner.getSelectedItem().toString());
        event.setHoraEvento(mHour);
        realm.copyToRealmOrUpdate(event);
        realm.commitTransaction();
    }
}
