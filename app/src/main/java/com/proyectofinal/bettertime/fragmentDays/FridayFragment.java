package com.proyectofinal.bettertime.fragmentDays;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proyectofinal.bettertime.DaysEventAdapter;
import com.proyectofinal.bettertime.R;
import com.proyectofinal.bettertime.UpdateEvento;
import com.proyectofinal.bettertime.model.DaysEvent;

import java.util.List;

import io.realm.Realm;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class FridayFragment extends Fragment implements DaysEventAdapter.OnEventClicked {

    private RecyclerView mDayReciclerView;
    DaysEventAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_friday, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDayReciclerView = (RecyclerView) getActivity().findViewById(R.id.rv_friday_events_list);

        adapter = new DaysEventAdapter(loadEventList(), this);
        mDayReciclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new DaysEventAdapter(loadEventList(), this);
        mDayReciclerView.setAdapter(adapter);
        Log.d("EventList", "Event saved: " + loadEventList());
    }

    private List<DaysEvent> loadEventList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DaysEvent.class).equalTo("diaEvento", "Viernes").findAll();
    }

    @Override
    public void onEventTapped(DaysEvent daysEvent, int position) {
        Intent intent = new Intent(getActivity(), UpdateEvento.class);
        intent.putExtra(DaysEvent.TAG, daysEvent);
        startActivity(intent);
    }
}
