package com.proyectofinal.bettertime.fragmentDays;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proyectofinal.bettertime.DaysEventAdapter;
import com.proyectofinal.bettertime.R;
import com.proyectofinal.bettertime.UpdateEvento;
import com.proyectofinal.bettertime.model.DaysEvent;

import java.util.List;

import io.realm.Realm;


public class SaturdayFragment extends Fragment implements DaysEventAdapter.OnEventClicked {

    public static SaturdayFragment getInstance() {
        return new SaturdayFragment();
    }

    private RecyclerView mDayReciclerView;
    DaysEventAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_saturday, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDayReciclerView = (RecyclerView) getActivity().findViewById(R.id.rv_saturday_events_list);

        adapter = new DaysEventAdapter(loadEventList(), this);
        mDayReciclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter = new DaysEventAdapter(loadEventList(), this);
        mDayReciclerView.setAdapter(adapter);
        Log.d("EventList", "Event saved: " + loadEventList());
    }

    private List<DaysEvent> loadEventList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(DaysEvent.class).equalTo("diaEvento", "Sabado").findAllSorted("horaEvento");
    }

    @Override
    public void onEventTapped(DaysEvent daysEvent, int position) {
        Intent intent = new Intent(getActivity(), UpdateEvento.class);
        intent.putExtra(DaysEvent.TAG, daysEvent);
        startActivity(intent);
    }
}