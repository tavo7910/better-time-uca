package com.proyectofinal.bettertime;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.proyectofinal.bettertime.model.Materias;

import java.util.List;

import io.realm.Realm;

/**
 * Created by gustavo.applaudo on 5/18/17.
 */

public class MateriasFragment extends Fragment implements MateriasAdapter.OnEventClicked {

    private RecyclerView recycleMateria;
    private MateriasAdapter materiasAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_materias, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.menu_subject));
        setHasOptionsMenu(true);
        recycleMateria = (RecyclerView) getActivity().findViewById(R.id.rv_all_materias_list);
        materiasAdapter = new MateriasAdapter(getMaterias(), this);
        recycleMateria.setAdapter(materiasAdapter);
        Log.d("materiasList", "list of materias: " + getMaterias());
    }

    @Override
    public void onResume() {
        super.onResume();
        materiasAdapter = new MateriasAdapter(getMaterias(), this);
        recycleMateria.setAdapter(materiasAdapter);
        Log.d("materiasList", "list of materias: " + getMaterias());
    }

    /** Show toolbar icons*/
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_materia, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /** Handle actions for toolbar icons*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_new_materia) {
            Intent intent = new Intent(getActivity(), CreateMateriaActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Materias> getMaterias() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Materias.class).findAll();
    }

    @Override
    public void onEventTapped(Materias materias, int position) {
        Toast.makeText(getActivity(), "Materia: " +materias.getNombreMateria(), Toast.LENGTH_SHORT).show();
    }
}
