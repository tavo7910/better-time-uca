package com.proyectofinal.bettertime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.proyectofinal.bettertime.model.DaysEvent;
import com.proyectofinal.bettertime.model.Materias;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class UpdateEvento extends AppCompatActivity {

    private Button btnSaveEvento;
    private DaysEvent daysEvent;
    private EditText eventName;
    private Spinner mDiaSpinner;
    private Spinner mMateriaSpinner;
    private TimePicker mHora;
    private String format = "";
    private String mHour = "";
    private TextView mHoraView;
    private List<String> weekDays = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_evento);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        daysEvent = getIntent().getExtras().getParcelable(DaysEvent.TAG);

        if (daysEvent != null) {
            Log.d("materiaData", "materia: " + daysEvent.getMateria());
        }

        eventName = (EditText) findViewById(R.id.nombre_evento_editText);

        btnSaveEvento = (Button) findViewById(R.id.btn_save_evento);
        mDiaSpinner = (Spinner) findViewById(R.id.dia_evento_editText);
        mMateriaSpinner = (Spinner) findViewById(R.id.materia_evento_spinner);
        mHora = (TimePicker) findViewById(R.id.hora_evento_editText);
        mHoraView = (TextView) findViewById(R.id.hora_evento_textView);

        spinnerItems();
        spinnerMateriaItems();
        getMaterias(loadMateriasList());

        if (daysEvent != null) {
            setEventData();
        }

        btnSaveEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHour();
                updateEvento();
                Toast.makeText(UpdateEvento.this, "Evento Actualizado", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void setEventData() {
        eventName.setText(daysEvent.getNombreEvento());
    }

    private void spinnerItems() {
        weekDays.add("Lunes");
        weekDays.add("Martes");
        weekDays.add("Miercoles");
        weekDays.add("Jueves");
        weekDays.add("Viernes");
        weekDays.add("Sabado");


        ArrayAdapter<String> daysSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, weekDays);
        daysSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDiaSpinner.setAdapter(daysSpinnerAdapter);

        setSelectedDay(mDiaSpinner, daysEvent, weekDays);
    }

    private void setSelectedDay(Spinner spinner, DaysEvent daysEvent, List<String> weekDays) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(daysEvent.getDiaEvento())) {
                spinner.setSelection(i);
            }
        }
        if (!spinner.getSelectedItem().toString().equals(daysEvent.getDiaEvento())) {
            spinner.setSelection(weekDays.indexOf(weekDays));
        }
    }


    private void spinnerMateriaItems() {
        ArrayAdapter<String> materiasSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,
                getMaterias(loadMateriasList()));
        materiasSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mMateriaSpinner.setAdapter(materiasSpinnerAdapter);

        setSelectedMateria(mMateriaSpinner, daysEvent);
    }

    private void setSelectedMateria(Spinner spinner, DaysEvent materia) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equals(materia.getMateria())) {
                spinner.setSelection(i);
            }
        }
    }

    private List<Materias> loadMateriasList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Materias.class).findAll();
    }

    private List<String> getMaterias(List<Materias> materiasList) {
        List<String> materiass = new ArrayList<>();
        for (Materias materias : materiasList) {
            Log.d("materia", "materia list: " + materias.getNombreMateria());
            materiass.add(materias.getNombreMateria());
        }
        return materiass;
    }

    private void showHour() {
        int hour = mHora.getCurrentHour();
        int min = mHora.getCurrentMinute();
        setHourFormat(hour, min);
    }

    private void setHourFormat(int hour, int min) {
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }

        //Log.d("timeFormatted", "hour is: " + new StringBuilder().append(hour).append(":").append(min).append(" ").append(format));
        mHoraView.setText(new StringBuilder().append(hour).append(":").append(min).append(" ").append(format));
        mHour = mHoraView.getText().toString();
    }

    private void updateEvento() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        DaysEvent event = realm.where(DaysEvent.class).equalTo("id", daysEvent.getId()).findFirst();
        event.setNombreEvento(eventName.getText().toString());
        event.setDiaEvento(mDiaSpinner.getSelectedItem().toString());
        event.setMateria(mMateriaSpinner.getSelectedItem().toString());
        event.setHoraEvento(mHour);
        realm.copyToRealmOrUpdate(event);

        realm.commitTransaction();
    }

    private void deleteEvent() {
        Realm realm = Realm.getDefaultInstance();

        final DaysEvent event = realm.where(DaysEvent.class).equalTo("id", daysEvent.getId()).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                event.deleteFromRealm();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete_evento, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_delete_event) {
            deleteEvent();
            Toast.makeText(this, "Evento Eliminado", Toast.LENGTH_SHORT).show();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
